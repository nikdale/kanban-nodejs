import React from 'react';

import NewUserModal from '../components/new-user.modal';
import { userService } from '../services/user.services';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // userService.logout();
        this.state = {
            showNewUserModal: false,
            // email: '',
            // password: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.showNewUsrModal = e => {
            this.setState({
                showNewUserModal: !this.state.showNewUserModal,
                show: false,
            })
        };
    }

    handleChange(e) {
        const { id, value } = e.target;
        this.setState({ [id]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { emailInput, passwordInput } = this.state;
        if (!(emailInput && passwordInput)) {
            return;
        }

        this.setState({ loading: true });
        userService.login(emailInput, passwordInput)
            .then(
                user => {
                    const { from } = this.props.location.state || { from: { pathname: "/" } };
                    this.props.history.push(from);
                },
                error => {
                    console.log(error);
                    this.setState({ error, loading: false })
                }
            );
    }

    render() {
        const { password, email } = this.state;
        return (
            <React.Fragment>
                <NewUserModal onClose={this.showNewUsrModal} showNewUserModal={this.state.showNewUserModal}>
                    Register new account
                </NewUserModal>

                <div className="col-12 col-md-9 col-xl-4 py-md-3 pl-md-5 bd-content">
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label className="">Email address</label>
                            <input type="email" className="form-control" id="emailInput" aria-describedby="emailHelp" placeholder="Enter email" value={email}
                                onChange={this.handleChange} />
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label className="">Password</label>
                            <input type="password" className="form-control" id="passwordInput" placeholder="Password" value={password}
                                onChange={this.handleChange} />
                        </div>
                        <div className="form-group form-check">
                            <input type="checkbox" className="form-check-input" id="rememberMe" />
                            <label className="form-check-label">Remember me</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                    <div className="form-group">
                        <p className="text-md-center" onClick={e => { this.showNewUsrModal(e); }} > No account? <button type="button" className="btn btn-link">Register</button></p>
                    </div>

                </div>
            </React.Fragment>
        );
    };
};

export { LoginPage }; 