import React from "react";
import '../style/modal.css';

export default class Modal extends React.Component {
    onClose = e => {
        this.props.onClose(e);
    };

    render() {
        if (!this.props.show) {
            return null;
        }
        console.log(this.props);
        return (

            <div className="modal1" id="modal1" userid={this.props.userid}>
                {/* <div className="modal-dialog" role="document"> */}
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{this.props.children}</h5>
                        <button type="button" className="close" aria-label="Close" onClick={e => { this.onClose(e); }}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="col-lg-8 push-lg-4 personal-info">
                            <form>
                                <div className="form-group row">
                                    <label className="col-lg-3 col-form-label form-control-label">First name</label>
                                    <div className="col-lg-9">
                                        <input className="form-control" type="text" value={this.props.userData.name} />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-lg-3 col-form-label form-control-label">Last name</label>
                                    <div className="col-lg-9">
                                        <input className="form-control" type="text" value={this.props.userData.surname} />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-lg-3 col-form-label form-control-label">Email</label>
                                    <div className="col-lg-9">
                                        <input className="form-control" type="email" value={this.props.userData.email} />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-lg-3 col-form-label form-control-label">Roles</label>
                                    <div className="col-lg-9">
                                        <select multiple id="user_roles" className="form-control" size="0">
                                            <option value="User">User</option>
                                            <option value="Admin">Admin</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary">Save changes</button>
                        <button type="button" className="btn btn-secondary" onClick={e => { this.onClose(e); }}>Close</button>
                    </div>
                </div>
                {/* </div> */}
            </div>
        );
    }
}
