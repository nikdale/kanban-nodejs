import React from "react";
import '../style/modal.css';
import { userService } from '../services/user.services';

export default class NewUserModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { id, value } = e.target;
        this.setState({ [id]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const {passwordInput, emailInput, rolesInput } = this.state;
        if (!(emailInput && passwordInput)) {
            return;
        }

        const userData = {
            "email": emailInput,
            "password": passwordInput
        };
        if (rolesInput && typeof(rolesInput) === Object) {
            userData.newRoles = rolesInput;
        }
        this.setState({ loading: true });
        userService.register(userData)
            .then(
                response => {
                this.onClose(e);
                alert(response.detail);
            },
            error => {
                console.log(error);
                alert(error);
                this.setState({ error, loading: false })
            }
        );
    }

    onClose = e => {
        this.props.onClose(e);
    };

    render() {
        if (!this.props.showNewUserModal) {
            return null;
        }

        const {password, email } = this.state;

        return (

            <div className="modal1" id="modal1" userid={this.props.userid}>
                {/* <div className="modal-dialog" role="document"> */}
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{this.props.children}</h5>
                        <button type="button" className="close" aria-label="Close" onClick={e => { this.onClose(e); }}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="col-lg-8 push-lg-4 personal-info">
                            <form onSubmit={this.handleSubmit} id="newUserForm">
                                <div className="form-group row">
                                    <label className="col-lg-3 col-form-label form-control-label">Email</label>
                                    <div className="col-lg-9">
                                        <input required id="emailInput" className="form-control" type="email" value={email} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-lg-3 col-form-label form-control-label">Password</label>
                                    <div className="col-lg-9">
                                        <input required id="passwordInput" className="form-control" type="password" value={password} onChange={this.handleChange} />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="submit" className="btn btn-primary" form="newUserForm" >Submit</button>
                        <button type="button" className="btn btn-secondary" onClick={e => { this.onClose(e); }}>Close</button>
                    </div>
                </div>
                {/* </div> */}
            </div>
        );
    }
}
