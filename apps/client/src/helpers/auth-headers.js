export function authHeader() {
    // return authorization header with basic auth credentials
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.authdata) {
        return { 
            'Authorization': 'Bearer ' + user.token,
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
        };
    } else {
        return {};
    }
}