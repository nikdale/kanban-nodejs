

const express = require('express');
const Roles = require('../common/enum');
const authorize = require('../common/role-auth');
const {registrationPost, loginPost, deleteUser, blockUser, updateUser, getAllUsers} = require('../controller/user.controller');

const router = express.Router();

router.post('/register', registrationPost);

router.post('/login', authorize(Roles.Admin), loginPost);

router.delete('/delete', authorize(Roles.Admin), deleteUser);

router.patch('/block', authorize(Roles.Admin), blockUser);

router.patch('/update', authorize(Roles.Admin), updateUser);

router.get('/getAll', authorize(Roles.Admin), getAllUsers);

module.exports = router;
