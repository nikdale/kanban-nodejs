const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const config = require('../config');
const User = require('../models/user.data');
const { isEmail } = require('../common/common');
const secret = config.jwt.secret;

exports.registrationPost = async (req, res, next) => {
    console.log('Došlo je do ovde')
    try {
        let { email, password } = req.body;
        if (!isEmail(email)) {
            throw new Error('Email must be a valid email address.');
        };
        if (typeof password !== 'string') {
            throw new Error('Password must be a string.');
        };
        let roles = [Roles.User];
        const user = await new User({ email, password});
        const persistedUser = await user.save();

        res.status(201).json({
            title: 'User Registration Successful',
            detail: 'Successfully registered new user',
        });
    } catch (err) {
        res.status(400).json({
            errors: [
                {
                    title: 'Registration Error',
                    detail: 'Something went wrong during registration process.',
                    errorMessage: err.message,
                },
            ],
        });
    };
};

exports.loginPost = async (req, res) => {
    try {
        const { email, password } = req.body;
        if (!isEmail(email)) {
            return res.status(400).json({
                errors: [
                    {
                        title: 'Bad Request',
                        detail: 'Email must be a valid email address',
                    },
                ],
            });
        }
        if (typeof password !== 'string') {
            return res.status(400).json({
                errors: [
                    {
                        title: 'Bad Request',
                        detail: 'Password must be a string',
                    },
                ],
            });
        }

        const user = await User.findOne({ email });
        if (!user) {
            throw new Error('Check email and password combination');
        }
        if (user.blocked) {
            throw new Error('You are blocked! Contact support.')
        }
        const passwordValidated = await bcrypt.compare(password, user.password);
        if (!passwordValidated) {
            throw new Error('Check email and password combination');
        }

        if (user) {
            const token = await jwt.sign({ sub: user.id, role: user.roles }, secret);
            const { password, ...userWithoutPassword } = user;
            data = {
                ...userWithoutPassword,
                token
            };
        }

        return res.json({
            title: 'Login Successful',
            detail: 'Successfully validated user credentials',
            token: data.token,
        });
    } catch (err) {
        return res.status(401).json({
            errors: [
                {
                    errorMessage: err.message,
                },
            ],
        });
    }
};

exports.deleteUser = async (req, res) => {
    try {
        const userId = req.query.id;

        if (!userId || !userId.match(/^[0-9a-fA-F]{24}$/)) {
            throw new Error('Id of user you are searching for is undefined');
        };

        const user = await User.findOne({ _id: userId });
        if (!user) {
            throw new Error('No user found.')
        }

        await User.findOneAndRemove({ _id: userId }, (err) => {
            if (err) {
                throw new Error(err);
            }
        });

        return res.status(201).json({
            title: 'User is deleted',
            detail: 'User has been deleted.',
        });
    } catch (err) {
        return res.status(401).json({
            errors: [
                {
                    errorMessage: err.message,
                },
            ],
        });
    }
};

exports.blockUser = async (req, res) => {
    try {
        const userId = req.query.id;
        let blockIt = true;
        let title = 'User Block Successful';
        let detail = 'Successfully blocked user';
        if (!userId || !userId.match(/^[0-9a-fA-F]{24}$/)) {
            throw new Error('Id of user you are searching for is undefined');
        };

        const user = await User.findOne({ _id: userId });
        if (!user) {
            throw new Error('No user found.')
        };
        if (user.blocked) {
            blockIt = false;
            title = 'User UnBlock Successful';
            detail = 'Successfully ublocked user';
        };

        const update = { blocked: blockIt, updatedAt: new Date() };
        const options = { upsert: true, new: true, setDefaultsOnInsert: true };

        await User.findOneAndUpdate({ _id: userId }, update, options, (err) => {
            if (err) {
                throw new Error(err);
            }
        });

        return res.status(201).json({
            title: title,
            detail: detail,
        });

    } catch (err) {
        return res.status(401).json({
            errors: [
                {
                    errorMessage: err.message,
                },
            ],
        });
    }
};

exports.updateUser = async (req, res) => {
    try {
        const userId = req.query.id;
        let { name, surname, email, password, newRoles } = req.body;

        if (!isEmail(email)) {
            throw new Error('Email must be a valid email address.');
        }
        if (typeof password !== 'string') {
            throw new Error('Password must be a string.');
        }
        if (name.length < 1) {
            throw new Error('Name cannot be empty.');
        }
        if (surname.length < 1) {
            throw new Error('Surname cannot be empty.');
        }

        if (!userId || !userId.match(/^[0-9a-fA-F]{24}$/)) {
            throw new Error('Id of user you are searching for is undefined');
        };

        const user = await User.findOne({ _id: userId });
        if (!user) {
            throw new Error('No user found.')
        };

        let roles = [Roles.User];
        if (!Array.isArray(newRoles)) {
            newRoles = [newRoles]
        }
        for (let [key, value] of Object.entries(Roles)) {
            for (let role of newRoles) {
                if (value.toLowerCase() === role.toLowerCase() && roles.findIndex(item => role.toLowerCase() === item.toLowerCase()) === -1) {
                    roles.push(value);
                }
            }
        }

        const update = { name, surname, email, password, roles, updatedAt: new Date() };
        const options = { upsert: true, new: true };

        await User.findOneAndUpdate({ _id: userId }, { $set: update }, options, (err) => {
            if (err) {
                throw new Error(err);
            }
        });

        return res.status(201).json({
            title: 'User Updated Successful',
            detail: 'Successfully updated user',
        });

    } catch (err) {
        return res.status(401).json({
            errors: [
                {
                    errorMessage: err.message,
                },
            ],
        });
    }
};

exports.getAllUsers = async (req, res) => {
    try {
        let sortData = {};
        let searchData = {};
        const name = req.query.nameSort,
            surname = req.query.surnameSort,
            email = req.query.emailSort;
        find = req.query.find;
        if (name) {
            sortData.name = name;
        }
        if (surname) {
            sortData.surname = surname;
        }
        if (email) {
            sortData.email = email;
        }
        if (find) {
            searchData = { $text: { $search: find } };
        }

        const users = await User.find(searchData, function (err, result) {
            if (err) {
                throw new Error(err);
            }
        }).sort(sortData);

        users.map(function (user) {
            user.password = undefined;
            return user;
        });

        return res.status(201).json(users);

    } catch (err) {
        return res.status(401).json({
            errors: [
                {
                    errorMessage: err.message,
                },
            ],
        });
    }
};