const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const TokenSchema = new mongoose.Schema({
    userId: {
        type: String,
        ref: "User"
    },
    token: {
        type: String,
        required: true,
        unique: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    usedAt: {
        type: Date,
        default: undefined
    }
});

// pushes inque plugin to schema
TokenSchema.plugin(uniqueValidator);

TokenSchema.pre('save', function (next) {
    let token = this;
    rand = Math.floor((Math.random() * 100) + 54);
    token.token = rand;
    next();
});

module.exports = mongoose.model('Token', TokenSchema);

