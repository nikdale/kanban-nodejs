const jwt = require('jsonwebtoken');

const config = require('../config');
const User = require('../models/user.data');


const secret = config.jwt.secret;

function authorize(roles = []) {
    if (typeof roles === 'string') {
        roles = [roles];
    }

    return [
        (req, res, next) => {
            const token = req.headers.authorization;
            if (!token) {
                return res.status(401).json({ message: 'Unauthorized' });
            };
            const parts = token.split(' ');
            const tokenData = parts[1];

            const decoded = jwt.verify(tokenData, secret);
            const userRoles = decoded.role;

            const includes = stringInArray(userRoles, roles);

            if (roles.length && !includes) {
                return res.status(401).json({ message: 'Unauthorized' });
            }
            next();
        }
    ];
};

function stringInArray(usrRoles, targetRoles) {
    var found = false;
    for (var i = 0; i < usrRoles.length; i++) {
        if (targetRoles.indexOf(usrRoles[i]) > -1) {
            found = true;
            break;
        }
    }
    return found;
};

module.exports = authorize;
